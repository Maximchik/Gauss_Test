package TestGaussian;

import java.io.*;

public class TestGaussian {
		//Main method
	
	public static final double epsilon = Double.MIN_VALUE;
	public static final double two_pi = 2.0*3.14159265358979323846;
	
	// Order size mean and std
    private static final double SizeMean = 100000;
    private static final double SizeSTD = 20000;
    
    // Order price mean and std
    private static final double PriceMean = 50;
    private static final double PriceSTD = 6;

	// These are thread local vars
	double z1 = 0;
    boolean generate = false;

	public double generateGaussianNoise(double mu, double sigma) {	
		generate = !generate;

        if (!generate)
           return z1 * sigma + mu;

        double u1, u2;
        do {
           u1 = Math.random();
           u2 = Math.random();
         } while ( u1 <= epsilon );

        double z0;
        z0 = Math.sqrt(-2.0 * Math.log(u1)) * Math.cos(two_pi * u2);
        z1 = Math.sqrt(-2.0 * Math.log(u1)) * Math.sin(two_pi * u2);
        return z0 * sigma + mu;
	}
	
    public static void main(String[] args) {
    	TestGaussian tg = new TestGaussian();
    	int max  =10000;
    	try {
	    	PrintWriter wr = new PrintWriter(new FileWriter("normal_dstribution_q.txt"));
	    	for(int i=0; i < max; i++) {
	    		wr.println(tg.generateGaussianNoise(SizeMean, SizeSTD));
	    	}
	    	wr.close();
    	}
    	catch (IOException ex){
    		ex.printStackTrace();
    	}
    }   
}