import numpy as np
from scipy.stats import norm
import matplotlib.pyplot as plt
import pandas as pd

data = pd.read_csv('normal_dstribution_price.txt')

# Fit a normal distribution to the data:
print(data.head())
mu, std = norm.fit(data)
print("Price normal distribution...");
print(mu, " ", std)

data = pd.read_csv('normal_dstribution_q.txt')

# Fit a normal distribution to the data:
print(data.head())
mu, std = norm.fit(data)
print("Size normal distribution...");
print(mu, " ", std)

